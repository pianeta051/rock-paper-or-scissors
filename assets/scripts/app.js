const PIEDRA = 'PIEDRA';
const PAPEL = 'PAPEL';
const TIJERA = 'TIJERA';
const LAGARTO = 'LAGARTO';
const SPOCK = 'SPOCK';
const GANA_JUGADOR = 'GANA_JUGADOR';
const GANA_MAQUINA = 'GANA_MAQUINA';
const EMPATE = 'EMPATE';
const opciones = [PIEDRA, PAPEL, TIJERA, LAGARTO, SPOCK];
const VICTORIAS_SEGUIDAS = 3;
let vidasJuego;

inicializarVidas();
let seguir;
let finalizar;
let seleccionJugador;
let seleccionMaquina;
let vidaMaquina = vidasJuego.jugador;
let vidaJugador = vidasJuego.maquina;
let victoriaJugador = 0;
let victoriaMaquina = 0;
let racha = [];
let contadorRacha = 0;
let historial =[];
let primeraEntradaNoLeida = 0;

mostrarVidas();

btnPiedra.addEventListener('click', evento => {
  jugarTurno(PIEDRA);
});

btnPapel.addEventListener('click', evento => {
  jugarTurno(PAPEL);
});

btnTijera.addEventListener('click', evento => {
  jugarTurno(TIJERA);
});

btnLagarto.addEventListener('click', evento => {
  jugarTurno(LAGARTO);
});

btnSpock.addEventListener('click', evento => {
  jugarTurno(SPOCK);
});

btnHistorial.addEventListener('click', evento => {
  for (let i = primeraEntradaNoLeida; i < historial.length; i++) {
    const entrada = historial[i];
    let textoEntrada = `[Entrada ${i}]\n`;
    for (const dato in entrada) {
      textoEntrada += `${dato}: ${entrada[dato]}\n`;
    }
    console.log(textoEntrada);
  }
  primeraEntradaNoLeida = historial.length;
});

function actualizarJuego(jugador,maquina) {
  mostrarTexto(jugador, maquina);
  mostrarVidas();
  finRondaJuego();
}

function añadirAlHistorial(vidas,quienGano, jugador, maquina) {
  const entrada = {
     vidasInicialJugador: vidasJuego.jugador,
     vidasInicialMaquina: vidasJuego.maquina,
     jugadorElige:jugador,
     maquinaElige: maquina,
     vidaJugador: vidaJugador,
     vidaMaquina: vidaMaquina,
     quienGano: quienGano
  };
    switch(quienGano) {
     case GANA_JUGADOR:
       entrada.quienGano = quienGano;
       entrada.jugadorElige = jugador;
       entrada.maquinaElige = maquina;
       entrada.vidaJugador = vidaJugador;
       entrada.vidaMaquina = vidaMaquina;
     case GANA_MAQUINA:
     break;
     case EMPATE:
        entrada.quienGano = EMPATE;
        entrada.jugadorElige = jugador;
        entrada.maquinaElige = maquina;
        entrada.vidaJugador = vidaJugador;
        entrada.vidaMaquina = vidaMaquina;
       break;
    }
  historial.push(entrada);
}

function eleccionMaquina() {
  const MAX= opciones.length -1;
  const respuestaMaquina = Math.ceil(Math.random() * MAX);
  return opciones[respuestaMaquina];
}

function empezarPartida() {
  habililarJuego();
  textoPantallaMaquina.innerHTML = "";
  textoPantallaJugador.innerHTML = "";
  contenedorResultado.innerHTML = "";
  vidaJugador = vidasJuego.jugador;
  vidaMaquina = vidasJuego.maquina;
  actualizarJuego("","");
  deshabilitarSeguirFinalizarBtn();
}

function estaEnRacha(quienGano){
  racha.push(quienGano);
  if (racha.length >= VICTORIAS_SEGUIDAS) {
    for (let i = racha.length - 1; i >= 0; i--) {
      if (racha[i]===quienGano) {
        contadorRacha++;
      }else {
        contadorRacha = 0;
        racha = [];
        mostrarTitulo();
      }
    }
  }
  if (contadorRacha >= VICTORIAS_SEGUIDAS) {
    if (quienGano === GANA_JUGADOR) {
      textoTitulo.innerHTML = "JUGADOR ESTA EN RACHA";
      pantallaTitulo.style.background = '#3399ff';
    } else {
      textoTitulo.innerHTML = "MAQUINA ESTA EN RACHA";
      pantallaTitulo.style.background = '#ff8566';
    }
  }
}

function inicializarVidas() {
  try {
    vidasJuego = valorVidas();
  }catch(e) {
    alert(e.mensaje);
    alert("El usuario introdujo "+ e.recibido);
  }
}

function finJuego() {
  contenedorResultado.innerHTML = "<h2>Partidas Maquina : </h2>"+victoriaMaquina+"<br>"+"<h2>Partidas Jugador : </h2>"+victoriaJugador
}

function finRondaJuego() {
  if (vidaMaquina === 0 || vidaJugador ===0) {
    if (vidaMaquina) {
      contenedorResultado.innerHTML = "<h2> L O   S I E N T O!!!! Has Perdido la Partida </h2>"
      victoriaMaquina +=1;
    } else {
      contenedorResultado.innerHTML = "<h2> F E L I C I D A D E S !!!! Has Ganado la Partida </h2>"
      victoriaJugador +=1;
    }
    limpiarPartida();
  }
}

function jugarTurno(eleccion) {
  seleccionJugador = eleccion;
  seleccionMaquina = eleccionMaquina();
  estaEnRacha(quienGana(seleccionJugador, seleccionMaquina));
  mostrarResultadoRonda(quienGana(seleccionJugador,seleccionMaquina));
  actualizarJuego(seleccionJugador,seleccionMaquina);
  añadirAlHistorial(vidasJuego, quienGana(seleccionJugador,seleccionMaquina), seleccionJugador,seleccionMaquina);
}
function limpiarPartida() {
  vidaJugador = "";
  vidaMaquina = "";
  victoriaMaquina = 0;
  textoPantallaMaquina.innerHTML = "";
  textoPantallaJugador.innerHTML = "";
  mostrarTitulo();
  mostrarVidas();
  deshabilitarJuego();
  contenedorResultado.style.visibility = 'visible';
  seguirOfinalizar();
}

function mostrarResultadoRonda(resultado) {
  if (resultado === GANA_JUGADOR) {
    contenedorResultado.innerHTML = "<h2> Has Ganado !...</h2>"+"<h2>"+seleccionJugador+"</h2>"+"<h2>"+"Gana a"+"</h2>"+"<h2>"+seleccionMaquina+"</h2>";
    if (contadorRacha >= VICTORIAS_SEGUIDAS) {
      vidaMaquina -= 2;
    } else {
      vidaMaquina -= 1;
    }

  } else if (resultado === GANA_MAQUINA) {
    contenedorResultado.innerHTML = "<h2> Has Perdido !...</h2>"+"<h2>"+seleccionJugador+"</h2>"+"<h2>"+"pierde con"+"</h2>"+"<h2>"+seleccionMaquina+"</h2>";;
    if (contadorRacha >= VICTORIAS_SEGUIDAS) {
      vidaJugador -= 2;
    } else {
      vidaJugador -= 1;
    }

  }else{
    contenedorResultado.innerHTML = "<h2>Has Empatado!</h2>";
  }
}

function mostrarTexto(jugador, maquina) {
  textoPantallaMaquina.innerHTML = maquina;
  textoPantallaJugador.innerHTML = jugador;
}

function mostrarTitulo() {
  pantallaTitulo.style.background = 'white';
  textoTitulo.innerHTML = "PIEDRA, PAPEL Y TIJERA";
}

function mostrarVidas() {
    spanVidaJugador.innerHTML = vidaJugador;
    spanVidaMaquina.innerHTML = vidaMaquina;
}

function quienGana(eleccionJugador, eleccionMaquina) {
  if (eleccionJugador === eleccionMaquina) {
    return EMPATE;
  } else if(eleccionJugador === TIJERA){
    if((eleccionMaquina === PAPEL) || (eleccionMaquina === LAGARTO)){
      return GANA_JUGADOR;
    }
    else {
      return GANA_MAQUINA;
    }
  }
  else if (eleccionJugador === PAPEL) {
    if((eleccionMaquina === PIEDRA) || (eleccionMaquina === SPOCK)){
      return GANA_JUGADOR;
    }
    else {
      return GANA_MAQUINA;
    }
  }
  else if (eleccionJugador == PIEDRA) {
    if((eleccionMaquina === TIJERA) || (eleccionMaquina === LAGARTO)){
      return GANA_JUGADOR;
    }
    else {
      return GANA_MAQUINA;
    }
  }
  else if (eleccionJugador == LAGARTO) {
    if((eleccionMaquina === PAPEL)|| (eleccionMaquina === SPOCK)){
        return GANA_JUGADOR;
      }
      else {
        return GANA_MAQUINA;
      }
  }
  else{
      if((eleccionMaquina === TIJERA) || (eleccionMaquina === PIEDRA)){
        return GANA_JUGADOR;
      }
      else {
        return GANA_MAQUINA;
      }
  }
}


function valorVidas() {
  let vidas = {};
  vidas.jugador = parseInt(prompt('Digite las vidas del JUGADOR (debe ser un número positivo)', '5'));
  vidas.maquina = parseInt(prompt('Digite las vidas del MAQUINA (debe ser un número positivo)', '5'));

  if (isNaN(vidas.jugador) || (vidas.jugador < 0)) {
    const error = {
      mensaje: 'El numero de vidas del Jugador deben ser un numero positivo',
      recibido: vidas
    };
    throw error;
  }
  else if (isNaN(vidas.maquina) || (vidas.maquina < 0)) {
    const error = {
      mensaje: 'El numero de vidas de la Maquina deben ser un numero positivo',
      recibido: vidas
    };
    throw error;
  }
  else {
    return vidas;
  }
}


function seguirOfinalizar() {
  seguir = document.createElement("BUTTON");
  const mostrarSeguir = document.createTextNode("Seguir");
  finalizar = document.createElement("BUTTON");
  const mostrarFinalizar = document.createTextNode("Finalizar");
  seguir.id = "btn-empezar";
  finalizar.id = "btn-finalizar";
  seguir.appendChild(mostrarSeguir);
  document.body.appendChild(seguir);
  finalizar.appendChild(mostrarFinalizar);
  document.body.appendChild(finalizar);
  seguir.addEventListener('click', evento => {
    inicializarVidas();
    empezarPartida();
  });

  finalizar.addEventListener('click', evento =>{
    contenedorResultado.style.visibility = 'visible';
    finJuego();
    const h1Adios = document.createElement("H1");
    h1Adios.id = 'adios';
    deshabilitarSeguirFinalizarBtn();
    const textoDespedida = document.createTextNode("G R A C I A S      P O R      J U G A R ");
    h1Adios.appendChild(textoDespedida);
    document.body.appendChild(h1Adios);
  });
}

function deshabilitarJuego() {
  pantallaJuego.style.visibility = 'hidden';
}

function habililarJuego() {
  pantallaJuego.style.visibility = 'visible';
}

function deshabilitarSeguirFinalizarBtn() {
  seguir.parentNode.removeChild(seguir);
  finalizar.parentNode.removeChild(finalizar);
}
function habilitarSeguirFinalizarBtn() {
  seguir.style.visibility = 'visible';
  finalizar.style.visibility = 'visible';
}
