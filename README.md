## What is it about
This is the typical Rock, Paper or Scissors game with an upgrade (Spock Lizard).
It will start asking for the number of lives of the machine and the player. And from there the player will have the turn to choose. Then the machine will make a random move and the system will calculate the winner of the game.

We can at any time check the history.



---

## What we will use

We will work with pure Javascript without the help of any Framework working with the following:
1. Declaration of variables.
2. Get items from the .js file.
3. Basic function.
4. Insertion and removal of DOM elements.
5. DOM manipulation.
6. Array iteration applying different loops
---

## Clone a repository

Clone the repository and feel free to check out all the code.
Download it by running the index.